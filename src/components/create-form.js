import React from "react";
import { Form, Field } from "formik";
import { AntInput } from "../CreateAntFields/CreateAntdFields";
import {} from "@ant-design/icons";

const createForm = ({ handleSubmit, submitCount,  }) => {
  
  
    <Form
    className="form-container"
    style={{ alignContent: "center" }}
    onSubmit={handleSubmit}>

      <label>Firstname</label>
      <Field
      name="firstname"
      placeholder="Firstname"
      component={AntInput}
      type="text"
      />

      <label>Lastname</label>
      <Field
      name="lastname"
      placeholder="Lastname"
      component={AntInput}
      type="text"
      />

      <label>Phone-number</label>
      <Field
      name="phone"
      placeholder="Phone"
      component={AntInput}
      type="text"
      />

      <label>E-mail</label>
      <Field
      name="email"
      placeholder="E-mail"
      component={AntInput}
      type="e-mail"
   
      />

      <button primary type="submit">
            Save
          </button>
    </Form>

  
};

export default createForm;
