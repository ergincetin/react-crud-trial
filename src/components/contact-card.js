import React, {useContext} from "react"
import {Card, Descriptions} from "antd"
import {UserOutlined, PhoneOutlined, MailOutlined} from "@ant-design/icons"
import { Link } from "react-router-dom";
import axios from "axios";
import { ContactContext } from "../context/contact-context";
import {flashErrorMessage} from "./flash-message"

export default function ContactCard ({contact}) {

    const [state, dispatch] = useContext(ContactContext);

    const cardTitle = (

    <p><UserOutlined/> {contact.name.first} {contact.name.last}</p>
)

const deleteContact = async id => {
    try {
      const response = await axios.delete(`http://localhost:3030/contacts/${id}`);
      dispatch({
        type: "DELETE_CONTACT",
        payload: response.data
      });
    } catch (error) {
      flashErrorMessage(dispatch, error)
    }
  }

    return (

<Card title={cardTitle()}>
    <Descriptions>
        <p><PhoneOutlined/> {contact.phone}</p>
        <p><MailOutlined/> {contact.email}</p>

    </Descriptions>
    <hr></hr>
    
    <div>
    <div className="ui two buttons">
          <button className="ant-btn ant-btn-primary" style={{color: "green"}} as={Link} to={`/contacts/edit/${contact._id}`}>
            Edit
          </button>
          <button className="ant-btn ant-btn-primary" style={{color: "red"}}  onClick={() => deleteContact(contact._id)}>
            Delete
          </button>
        </div>
    </div>
</Card>


    )


}