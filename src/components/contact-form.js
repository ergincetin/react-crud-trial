import React, { useContext, useState } from "react";
import { useForm } from "react-hook-form";
import axios from "axios";
import { Redirect } from "react-router";
import { ContactContext } from "../context/contact-context";
import CreateForm from "./create-form"
import {Row, Col} from "antd" 
import FlashMessage, { flashErrorMessage } from "./flash-message";

export default function ContactForm({contact}) {
  const [state, dispatch] = useContext(ContactContext);
  const { register, errors, handleSubmit } = useForm({
    defaultValues: contact
  });
  const [redirect, setRedirect] = useState(false);

  const createContact = async data => {
    try {
      const response = await axios.post("http://localhost:3030/contacts", data);
      dispatch({
        type: "CREATE_CONTACT",
        payload: response.data
      });
      setRedirect(true);
    } catch (error) {
      flashErrorMessage(dispatch, error)
    }
  };

  const updateContact = async data => {
    try {
      const response = await axios.patch(
        `http://localhost:3030/contacts/${contact._id}`,
        data
      );
      dispatch({
        type: "UPDATE_CONTACT",
        payload: response.data
      });
      setRedirect(true);
    } catch (error) {
      flashErrorMessage(dispatch, error);
    }
  };

  const onSubmit = async data => {
    if(contact._id) {
      await updateContact(data);
    }
    else{
      await createContact(data);
    }
  };

  if (redirect) {
    return <Redirect to='/' />;
  }

  return (
    <Row>
      <Col>
      <h1 style={{ marginTop: "1em" }}>
          {contact._id ? "Edit Contact" : "Add New Contact"}
        </h1>
        {state.message.content && <FlashMessage message={state.message}/>}
      <CreateForm onSubmit={handleSubmit(onSubmit)} loading={state.loading} />
      </Col>
    </Row>
    
    
  );
}
 
