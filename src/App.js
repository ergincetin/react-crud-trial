import React, { PureComponent } from "react"
import ContactListPage from './pages/contact-list-page';
import ContactFormPage from './pages/contact-form-page';
import {Layout, Menu, Header, Content} from "antd"
import {Route} from "react-router-dom"
import {Link} from "react-router-dom"

class App extends PureComponent {

    render() {

        return (
            <Layout>
                <Header>
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={[""]}>
            <Menu.Item key="1" >
                <Link exact to="/">Contact List</Link>
            </Menu.Item>

            <Menu.Item key="2">
                <Link exact to="/contacts/new">Add New Contact</Link>
            </Menu.Item>
        </Menu>
        </Header>

        <Route exact path="/" component={ContactListPage}/>
        <Route path="/contacts/new" component={ContactFormPage}/>
        <Route path="/contacts/edit/:_id" component={ContactFormPage}/>

    </Layout>
        )
    }
    
}

export default App;